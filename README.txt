

--- README  -------------------------------------------------------------

External Link Page

Written by Alan Palazzolo, aka, zzolo (binarybill)
  http://zzolo.org



--- INSTALLATION --------------------------------------------------------

1. Place the ext_link_page folder in your modules directory.

2. Enable "External Link Page" under Administer > Site building > Modules.

3. Goto Administer > Site configuration > External link page to adjust settings.

4. Goto Administer > Site configuration > Input formats and click "configure" next to
   an input format you want to add this filter to.

5. Enable the "External links page".

6. Ensure permissions are set.  The "access redirect page" is needed to be redirected.



--- CHANGELOG --------------------------------------------------------
